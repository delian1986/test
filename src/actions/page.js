export const setPage = (value) => ({
  type: "SET_PAGE",
  payload: value
});
